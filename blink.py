import RPi.GPIO as GPIO
from time import sleep
 
LED1 = 22
LED2 = 17
LED3 = 4
BUTTON1 = 25
BUTTON2 = 24
BUTTON3 = 23
 
PAUSE_TIME = 0.1

print "Setting up GPIO"
GPIO.setmode(GPIO.BCM)
GPIO.setup(LED1, GPIO.OUT)
GPIO.setup(LED2, GPIO.OUT)
GPIO.setup(LED3, GPIO.OUT)
GPIO.setup(BUTTON1, GPIO.IN)
GPIO.setup(BUTTON2, GPIO.IN)
GPIO.setup(BUTTON3, GPIO.IN)
 
def enable_led(led_pin,should_enable):
	if should_enable:
		GPIO.output(led_pin, True)
		#~ print("LED", led_pin, "is on") 
	else:
		GPIO.output(led_pin, False)
		#~ print("LED", led_pin, "is off") 

while True:
	if ( GPIO.input(BUTTON1) == True ):
		print("Button 1 pressed!")
		print("Blinking LEDs")

		enable_led(LED1, True)
		sleep(PAUSE_TIME)
		enable_led(LED1, False)
		enable_led(LED2, True)
		sleep(PAUSE_TIME)
		enable_led(LED2, False)
		enable_led(LED3, True)
		sleep(PAUSE_TIME)
		enable_led(LED3, False)
		sleep(PAUSE_TIME)	
		enable_led(LED2, True)
		sleep(PAUSE_TIME)
		enable_led(LED2, False)
		enable_led(LED1, True)
		sleep(PAUSE_TIME)
		enable_led(LED1, False)

	if ( GPIO.input(BUTTON2) == True ):
		print("Button 2 pressed!")
		print("Blinking LEDs")

		enable_led(LED1, True)
		#~ enable_led(LED2, True)
		enable_led(LED3, True)
		sleep(PAUSE_TIME + 0.015)
		enable_led(LED1, False)
		#~ enable_led(LED2, False)
		enable_led(LED3, False)
		sleep(PAUSE_TIME + 0.015)
		
		enable_led(LED2, True)
		sleep(PAUSE_TIME + 0.015)
		enable_led(LED2, False)
		sleep(PAUSE_TIME + 0.015)

		enable_led(LED1, True)
		#~ enable_led(LED2, True)
		enable_led(LED3, True)
		sleep(PAUSE_TIME + 0.015)
		enable_led(LED1, False)
		#~ enable_led(LED2, False)
		enable_led(LED3, False)
		sleep(PAUSE_TIME + 0.015)

	if ( GPIO.input(BUTTON3) == True ):
		print("Button 3 pressed!")
		print("Blinking LEDs")

		enable_led(LED3, True)
		sleep(PAUSE_TIME)
		enable_led(LED3, False)
		enable_led(LED2, True)
		sleep(PAUSE_TIME)
		enable_led(LED2, False)
		enable_led(LED1, True)
		sleep(PAUSE_TIME)
		enable_led(LED1, False)
		sleep(PAUSE_TIME)
		enable_led(LED2, True)
		sleep(PAUSE_TIME)
		enable_led(LED2, False)
		sleep(PAUSE_TIME)
		enable_led(LED3, True)
		sleep(PAUSE_TIME)
		enable_led(LED3, False)

	if ( GPIO.input(BUTTON1) == True and GPIO.input(BUTTON3) == True ):
		print("Buttons 2 & 3 pressed!")
		print("Exiting")
		enable_led(LED1, False)
		enable_led(LED2, False)
		enable_led(LED3, False)
		GPIO.cleanup()
		exit()

#~ while True:
	#~ if ( GPIO.input(BUTTON1   ) == True ):
		#~ print("Button pressed!")
		#~ print("Blinking LEDs")

		#~ n = 0
		#~ while n < 5:
			#~ enable_led(LED1, True)
			#~ enable_led(LED2, True)
			#~ enable_led(LED3, True)
			#~ sleep(PAUSE_TIME + 0.015)
			#~ enable_led(LED1, False)
			#~ enable_led(LED2, False)
			#~ enable_led(LED3, False)
			#~ sleep(PAUSE_TIME + 0.015)
			#~ n = n + 1

		#~ n = 0
		#~ while n < 10:
			#~ enable_led(LED1, True)
			#~ sleep(PAUSE_TIME)
			#~ enable_led(LED1, False)
			#~ enable_led(LED2, True)
			#~ sleep(PAUSE_TIME)
			#~ enable_led(LED2, False)
			#~ enable_led(LED3, True)
			#~ sleep(PAUSE_TIME)
			#~ enable_led(LED3, False)
			#~ n = n + 1

		#~ n = 0
		#~ while n < 5:
			#~ enable_led(LED1, True)
			#~ enable_led(LED2, True)
			#~ enable_led(LED3, True)
			#~ sleep(PAUSE_TIME + 0.015)
			#~ enable_led(LED1, False)
			#~ enable_led(LED2, False)
			#~ enable_led(LED3, False)
			#~ sleep(PAUSE_TIME + 0.015)
			#~ n = n + 1
		
		#~ GPIO.cleanup()
		#~ exit()
		